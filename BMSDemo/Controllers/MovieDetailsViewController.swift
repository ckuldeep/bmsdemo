//
//  MovieDetailsViewController.swift
//  BMSDemo
//
//  Created by Kuldeep on 15/10/20.
//  Copyright © 2020 KULDEEP. All rights reserved.
//

import UIKit

private enum MovieInfoCellType {
    case info
    case reviews
    case credits
    case similar
}

private struct MovieDetailsDataSetup {
    var cellType: MovieInfoCellType
    var model: Any?
    
    init(cellType: MovieInfoCellType, model: Any?) {
        self.cellType = cellType
        self.model = model
    }
}

class MovieDetailsViewController: UIViewController {

    var movieDataVM: MovieDataViewModel?
    private var reviewsRequestVM = MovieReviewRequestVM()
    var reviewsVM = ReviewsViewModel()
    
    private var dataSourceArray = [[MovieDetailsDataSetup]]()

    
    @IBOutlet weak var _movieBackDropImg: UIImageView!
    @IBOutlet weak var _verticalDistanceTblViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var _downBtn: UIButton!
    @IBOutlet weak var _movieInfoTblView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupInitialUI()
    }
    
    private func setupInitialUI() {
        self.navigationController?.navigationBar.isHidden = true
        let imgUrl = URL.posterImgURL(movieDataVM?.posterImg, size: .poster)
        _movieBackDropImg.loadImageAsync(with: imgUrl)
        _downBtn.isHidden = true
        setupTableView()
        getMovieReviews()
    }
    
    private func setupTableView() {
        registerCells()
        setCornerRadiiOnTable()
        _movieInfoTblView.sectionHeaderHeight = UITableView.automaticDimension
        _movieInfoTblView.estimatedSectionHeaderHeight = 200
        _movieInfoTblView.rowHeight = UITableView.automaticDimension
        _movieInfoTblView.estimatedRowHeight = 200
        createDataSourceForAll()
    }
    
    private func setCornerRadiiOnTable() {
        _movieInfoTblView.clipsToBounds = true
        _movieInfoTblView.layer.cornerRadius = 40
        _movieInfoTblView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
    }
    
    @IBAction func downBtnTapped(_ sender: Any) {
        self.animateTableWith(-50, hideBtn: true)
    }
    
    private func animateTableWith(_ value: CGFloat, hideBtn isHide: Bool) {
        self._verticalDistanceTblViewConstraint.constant = value
        UIView.animate(withDuration: 0.5, animations: {
                self.view.layoutIfNeeded()
            }, completion: {res in
                self._downBtn.isHidden = isHide
        })
    }
    
    private func getMovieReviews() {
        self.reviewsRequestVM.apiKey = APIKey.key
        self.reviewsRequestVM.movieId = movieDataVM?.movieId
        
        if(self.reviewsRequestVM.validate()) {
            // get reviews
            DispatchQueue.global(qos: .userInitiated).async {
                Webservice().load(resource: MovieReviewRequest.create(vm: self.reviewsRequestVM)) { [weak self] result in
                    switch result {
                        case .success(let reviews):
                            if let reviews = reviews {
                                if reviews.totalResults != 0 {
                                    self?.reviewsVM.reviewsViewModel = reviews.results.map(ReviewDataViewModel.init)
                                    DispatchQueue.main.async {
                                        self?.createDataSourceForReviews()
                                    }
                                }
                            }
                        case .failure(let error):
                            print(error)
                    }
                }
            }
        }
    }
}

extension MovieDetailsViewController {
    private func registerCells() {
        MovieInfoTableViewCell.registerNibForTable(_movieInfoTblView)
        MovieReviewTableViewCell.registerNibForTable(_movieInfoTblView)
    }
    
    private func createDataSourceForAll() {
        createDataSourceForInfo()
        createDataSourceForReviews()
        _movieInfoTblView.reloadData()
    }
    
    private func createDataSourceForInfo() {
        let datasetup = MovieInfoCellDataSetup(movieName: movieDataVM?.movieName, overview: movieDataVM?.movieOverview)
        createDataSource(cellType: .info, dataSetup: datasetup)
    }
    
    private func createDataSourceForReviews() {
        var dataSetup = MovieReviewDataSetup(reviews: [])
        if self.reviewsVM.reviewsViewModel.count != 0 {
            if let section = self.dataSourceArray.firstIndex(where: {$0.first?.cellType == .reviews}) {
                dataSetup = MovieReviewDataSetup(reviews: self.reviewsVM.reviewsViewModel)
                self.dataSourceArray.remove(at: section)
                self.createDataSource(cellType: .reviews, dataSetup: dataSetup, sectionNumber: section)
                self._movieInfoTblView.reloadData()
            }
        }
        createDataSource(cellType: .reviews, dataSetup: nil)
    }
    
    private func createDataSource(cellType: MovieInfoCellType, dataSetup: Any?, sectionNumber: Int? = nil) {
        var section: [MovieDetailsDataSetup] = []
        let row = MovieDetailsDataSetup(cellType: cellType, model: dataSetup)
        section.append(row)
        guard let sectionNumberUW = sectionNumber else {
            dataSourceArray.append(section)
            return
        }
        dataSourceArray.insert(section, at: sectionNumberUW)
    }
}

extension MovieDetailsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        let translation = scrollView.panGestureRecognizer.translation(in: scrollView.superview)
        if !(translation.y > 0) {
            // swipes from bottom to top of screen -> up
            self.animateTableWith(-self._movieBackDropImg.frame.size.height + 50, hideBtn: false)

        }
    }
}

extension MovieDetailsViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return dataSourceArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSourceArray[section].count - 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let dataSetup = dataSourceArray[indexPath.section][indexPath.row + 1]
        switch dataSetup.cellType {
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let dataSetup = dataSourceArray[section]
        switch dataSetup.first?.cellType {
        case .info:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: MovieInfoTableViewCell.ReuseIdentifier) as? MovieInfoTableViewCell else {
                return UIView()
            }
            cell.setupCell(dataSetup: dataSetup.first?.model as? MovieInfoCellDataSetup)
            return cell
        case .reviews:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: MovieReviewTableViewCell.ReuseIdentifier) as? MovieReviewTableViewCell else {
                return UIView()
            }
            cell.setupCell(dataSetup: dataSetup.first?.model as? MovieReviewDataSetup)
            return cell
        default:
            return UIView()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let dataSetup = dataSourceArray[section]
        switch dataSetup.first?.cellType {
        case .reviews:
            return 200
        default:
            return UITableView.automaticDimension
        }
    }

}
