//
//  SearchViewController.swift
//  BMSDemo
//
//  Created by Kuldeep on 15/10/20.
//  Copyright © 2020 KULDEEP. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController {

    var moviesListViewModel = MoviesListViewModel()

    private var sortedList = [MovieDataViewModel]()
    
    @IBOutlet weak var _searchBar: UISearchBar!
    @IBOutlet weak var _searchResultTbl: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        sortedList = self.moviesListViewModel.moviesViewModel
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
}

extension SearchViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
}

extension SearchViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sortedList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "SearchCell") as? SearchCell else {
            return UITableViewCell()
        }
        cell.selectionStyle = .none
        let movie = sortedList[indexPath.row]
        let data = SearchCellDataSetup(movieName: movie.movieName, movieImg: movie.posterImg)
        cell.setupCell(dataSetup: data)
        return UITableViewCell()
    }
    
    
}
