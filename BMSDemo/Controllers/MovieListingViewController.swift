//
//  MovieListingViewController.swift
//  BMSDemo
//
//  Created by Kuldeep on 15/10/20.
//  Copyright © 2020 KULDEEP. All rights reserved.
//

import UIKit

class MovieListingViewController: UIViewController {

    var didSelect: (MovieDataViewModel) -> () = { _ in }

    private var vm = NowPlayingRequestVM()
    var moviesListViewModel = MoviesListViewModel()
    
    @IBOutlet weak var _movieListTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getMoviesList()
        MovieListTableViewCell.registerNibForTable(_movieListTableView)

        // Do any additional setup after loading the view.
    }
    
    private func getMoviesList() {
        self.vm.apiKey = APIKey.key
        //self.vm.region = Region.india
        if(self.vm.validate()) {
            // save the order
            Webservice().load(resource: NowPlayingRequest.create(vm: self.vm)) { [weak self] result in
                switch result {
                    case .success(let movieList):
                        if let movieList = movieList {
                            guard let results = movieList.results else {
                                return
                            }
                            self?.moviesListViewModel.moviesViewModel = results.map(MovieDataViewModel.init)
                            DispatchQueue.main.async {
                                self?._movieListTableView.reloadData()
                            }
        
                        }
                    case .failure(let error):
                        print(error)
                }
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == MovieDetailsViewController.SegueIdentifier {
            let vc = (segue.destination as! UINavigationController)
                .topViewController as! MovieDetailsViewController
            
            vc.movieDataVM = sender as? MovieDataViewModel
        } else {
            guard let navC = segue.destination as? UINavigationController,
                let searchvc = navC.viewControllers.first as? SearchViewController else {
                    fatalError("Controller not found")
            }
            searchvc.moviesListViewModel = self.moviesListViewModel
        }
    }

}

extension MovieListingViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let moviesViewModel = self.moviesListViewModel.moviesViewModel[indexPath.row]
        self.performSegue(withIdentifier: MovieDetailsViewController.SegueIdentifier, sender: moviesViewModel)
    }
}

extension MovieListingViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.moviesListViewModel.moviesViewModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: MovieListTableViewCell.ReuseIdentifier) as? MovieListTableViewCell else {
            return UITableViewCell()
        }
        cell.selectionStyle = .none
        let moviedata = self.moviesListViewModel.moviesViewModel[indexPath.row]
        let dataSetup = MovieListCellDataSetup(movieName: moviedata.movieName, movieImg: moviedata.posterImg, releaseDate: moviedata.releaseDate)
        cell.setupCell(dataSetup: dataSetup) {
            // handle book button tap
        }
        
        return cell
    }
}
