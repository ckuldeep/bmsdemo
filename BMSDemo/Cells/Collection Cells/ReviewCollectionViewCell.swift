//
//  ReviewCollectionViewCell.swift
//  BMSDemo
//
//  Created by Kuldeep on 17/10/20.
//  Copyright © 2020 KULDEEP. All rights reserved.
//

import UIKit

struct ReviewCollectionCellDataSetup {
    let author: String?
    let review: String?
}

class ReviewCollectionViewCell: UICollectionViewCell, ReusableCellProtocol {

    lazy var width: NSLayoutConstraint = {
        let width = contentView.widthAnchor.constraint(equalToConstant: bounds.size.width)
        width.isActive = true
        return width
    }()
    
      @IBOutlet weak var _authorNameLbl: UILabel!
      @IBOutlet weak var _review: UILabel!
      
      override func awakeFromNib() {
        super.awakeFromNib()
        contentView.translatesAutoresizingMaskIntoConstraints = false

      }
      
    override func systemLayoutSizeFitting(_ targetSize: CGSize, withHorizontalFittingPriority horizontalFittingPriority: UILayoutPriority, verticalFittingPriority: UILayoutPriority) -> CGSize {
        width.constant = bounds.size.width
        return contentView.systemLayoutSizeFitting(CGSize(width: targetSize.width, height: 1))
    }
    
    func setupCell(dataSetup: ReviewCollectionCellDataSetup?) {
        guard let data = dataSetup else {
            return
        }
        _authorNameLbl.text = data.author
        _review.text = data.review
    }
    
}
