//
//  MovieListTableViewCell.swift
//  BMSDemo
//
//  Created by Kuldeep on 16/10/20.
//  Copyright © 2020 KULDEEP. All rights reserved.
//

import UIKit

struct MovieListCellDataSetup {
    let movieName: String?
    let movieImg: String?
    let releaseDate: String?
}

class MovieListTableViewCell: UITableViewCell, ReusableCellProtocol {

    private var didTapBook: (() -> ())?

    @IBOutlet weak var _movieImg: UIImageView!
    @IBOutlet weak var _movieNameLbl: UILabel!
    @IBOutlet weak var _releaseDateLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func setupCell(dataSetup: MovieListCellDataSetup?, actionCallBack: (() -> Void)?) {
        guard let data = dataSetup else {
            return
        }
        didTapBook = actionCallBack
        _movieNameLbl.text = data.movieName
        if let date = data.releaseDate {
            _releaseDateLbl.text = "Releasing date " + date
        }
        let imgUrl = URL.posterImgURL(data.movieImg, size: .tile)
        _movieImg.loadImageAsync(with: imgUrl)

    }
    
    @IBAction func bookBtnTapped(_ sender: Any) {
        didTapBook?()
    }
    
}
