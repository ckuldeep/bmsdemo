//
//  MovieReviewTableViewCell.swift
//  BMSDemo
//
//  Created by Kuldeep on 17/10/20.
//  Copyright © 2020 KULDEEP. All rights reserved.
//

import UIKit

struct MovieReviewDataSetup {
    let reviews: [ReviewDataViewModel]?
}

class MovieReviewTableViewCell: UITableViewCell, ReusableCellProtocol {

    var layout: UICollectionViewFlowLayout = {
        let layout = UICollectionViewFlowLayout()
        let width = UIScreen.main.bounds.size.width
        layout.estimatedItemSize = CGSize(width: width, height: 10)
        return layout
    }()
    
    private var reviewsArr: [ReviewDataViewModel]?
    
    @IBOutlet weak var _reviewsCollection: UICollectionView!
    @IBOutlet weak var _noReviewsLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        _reviewsCollection.isHidden = true
        ReviewCollectionViewCell.registerNibForCollection(_reviewsCollection)
        _reviewsCollection.collectionViewLayout = layout
        
//        if let flowLayout = _reviewsCollection?.collectionViewLayout as? UICollectionViewFlowLayout {
//            flowLayout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
//        }
    }
    
    func setupCell(dataSetup: MovieReviewDataSetup?) {
       guard let data = dataSetup else {
           return
       }
        if data.reviews!.count > 0 {
            reviewsArr = data.reviews
            _reviewsCollection.isHidden = false
            _noReviewsLbl.isHidden = true
            _reviewsCollection.reloadData()
        }
    }
    
}

extension MovieReviewTableViewCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ReviewCollectionViewCell.ReuseIdentifier, for: indexPath) as? ReviewCollectionViewCell else {
            return UICollectionViewCell()
        }
        let review = reviewsArr![indexPath.row]
        let datasetup = ReviewCollectionCellDataSetup(author: review.author, review: review.content)
        cell.setupCell(dataSetup: datasetup)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return reviewsArr?.count ?? 0
    }
}

extension MovieReviewTableViewCell: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: collectionView.bounds.width, height: collectionView.bounds.height)
    }
}
