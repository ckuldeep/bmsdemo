//
//  SearchCell.swift
//  BMSDemo
//
//  Created by Kuldeep on 17/10/20.
//  Copyright © 2020 KULDEEP. All rights reserved.
//

import UIKit

struct SearchCellDataSetup {
    let movieName: String?
    let movieImg: String?
}

class SearchCell: UITableViewCell {
    
    @IBOutlet weak var _searchItemImg: UIImageView!
    @IBOutlet weak var _searchCellLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func setupCell(dataSetup: SearchCellDataSetup?) {
        guard let data = dataSetup else {
            return
        }
        _searchCellLbl.text = data.movieName
        let imgUrl = URL.posterImgURL(data.movieImg, size: .tile)
        _searchItemImg.loadImageAsync(with: imgUrl)

    }
}
