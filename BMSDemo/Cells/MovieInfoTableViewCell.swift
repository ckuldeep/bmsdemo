//
//  MovieInfoTableViewCell.swift
//  BMSDemo
//
//  Created by Kuldeep on 17/10/20.
//  Copyright © 2020 KULDEEP. All rights reserved.
//

import UIKit

struct MovieInfoCellDataSetup {
    let movieName: String?
    let overview: String?
}

class MovieInfoTableViewCell: UITableViewCell, ReusableCellProtocol {

    @IBOutlet weak var _movieNameLbl: UILabel!
    @IBOutlet weak var _movieOverviewLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setupCell(dataSetup: MovieInfoCellDataSetup?) {
        guard let data = dataSetup else {
            return
        }
        _movieNameLbl.text = data.movieName
        _movieOverviewLbl.text = data.overview
    }
    
}
