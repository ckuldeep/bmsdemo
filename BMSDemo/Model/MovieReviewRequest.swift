//
//  MovieReviewRequest.swift
//  BMSDemo
//
//  Created by Kuldeep on 17/10/20.
//  Copyright © 2020 KULDEEP. All rights reserved.
//

import Foundation

struct MovieReviewRequest: Codable {
    let apiKey: String
    let id: Int?
    
    enum CodingKeys: String, CodingKey {
           case apiKey = "api_key"
           case id = "movie_id"
    }
}

extension MovieReviewRequest {
    
    static func create(vm: MovieReviewRequestVM) -> Resource<MovieReview?> {
        
        let request = MovieReviewRequest(vm)
        
        let params = request.dictionary!
        guard let urlStr = URL.reviews else {
            fatalError("URL is not defined!")
        }
        let id = params["movie_id"]! as? Int
        let updateUrl = urlStr.replacingOccurrences(of: "{Movie_Id}", with: String(id!))
        
        var urlComponents = URLComponents(url: URL(string: updateUrl)!, resolvingAgainstBaseURL: true)

        urlComponents?.queryItems = [URLQueryItem(name: "api_key", value: params["api_key"] as? String)]
        
        var resource = Resource<MovieReview?>(url: (urlComponents?.url)!)
        resource.httpMethod = HttpMethod.get
        return resource
        
    }
    
}

extension MovieReviewRequest {
    init?(_ vm: MovieReviewRequestVM) {
        guard let apiKey = vm.apiKey, let id = vm.movieId else {
                return nil
        }

        self.apiKey = apiKey
        self.id = id
    }
    
}
