//
//  MovieReviewModel.swift
//  BMSDemo
//
//  Created by Kuldeep on 17/10/20.
//  Copyright © 2020 KULDEEP. All rights reserved.
//

import Foundation

// MARK: - MovieReview
struct MovieReview: Codable {
    let id, page: Int
    let results: [ReviewResult]
    let totalPages, totalResults: Int

    enum CodingKeys: String, CodingKey {
        case id, page, results
        case totalPages = "total_pages"
        case totalResults = "total_results"
    }
}

// MARK: - ReviewResult
struct ReviewResult: Codable {
    let author, content, id: String
    let url: String
}
