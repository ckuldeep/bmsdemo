//
//  NowPlayingRequest.swift
//  BMSDemo
//
//  Created by Kuldeep on 16/10/20.
//  Copyright © 2020 KULDEEP. All rights reserved.
//

import Foundation

struct NowPlayingRequest: Codable {
    let apiKey: String
    let region: String?
    
    enum CodingKeys: String, CodingKey {
           case apiKey = "api_key"
           case region
    }
}

extension Encodable {
  var dictionary: [String: Any]? {
    guard let data = try? JSONEncoder().encode(self) else { return nil }
    return (try? JSONSerialization.jsonObject(with: data, options: .allowFragments)).flatMap { $0 as? [String: Any] }
  }
}


extension NowPlayingRequest {
    
    static func create(vm: NowPlayingRequestVM) -> Resource<MovieResults?> {
        
        let request = NowPlayingRequest(vm)
        let params = request.dictionary as! [String:String]
        guard let url = URL.nowPlaying else {
            fatalError("URL is not defined!")
        }
        var urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: true)

        urlComponents?.queryItems = params.map { (key, value) in
            URLQueryItem(name: key, value: value)
        }
        
        var resource = Resource<MovieResults?>(url: (urlComponents?.url)!)
        resource.httpMethod = HttpMethod.get
        return resource
        
    }
    
}

extension NowPlayingRequest {
    
    init?(_ vm: NowPlayingRequestVM) {
        guard let apiKey = vm.apiKey else {
                return nil
        }

        self.apiKey = apiKey
        self.region = vm.region
    }
    
}
