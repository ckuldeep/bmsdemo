//
//  ReviewsViewModel.swift
//  BMSDemo
//
//  Created by Kuldeep on 17/10/20.
//  Copyright © 2020 KULDEEP. All rights reserved.
//

import Foundation

class ReviewsViewModel {
    
    var reviewsViewModel: [ReviewDataViewModel]
    
    init() {
        self.reviewsViewModel = [ReviewDataViewModel]()
    }
}

extension ReviewsViewModel {
    
    func reviewDataViewModel(at index: Int) -> ReviewDataViewModel {
        return self.reviewsViewModel[index]
    }
    
}

struct ReviewDataViewModel {
    let reviewData: ReviewResult
}

extension ReviewDataViewModel {
    var author: String {
        return self.reviewData.author
    }
    
    var content: String {
        return self.reviewData.content
    }
    
}
