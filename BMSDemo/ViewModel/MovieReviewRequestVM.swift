//
//  MovieReviewRequestVM.swift
//  BMSDemo
//
//  Created by Kuldeep on 17/10/20.
//  Copyright © 2020 KULDEEP. All rights reserved.
//

import Foundation

struct MovieReviewRequestVM: Validatable {
    var apiKey: String?
    var movieId: Int? = -1
    
    func validate() -> Bool {

        guard let apiKey = self.apiKey, let id = movieId else {
            return false
        }
        return !((apiKey.isEmpty) || (id == -1))
    }

}
