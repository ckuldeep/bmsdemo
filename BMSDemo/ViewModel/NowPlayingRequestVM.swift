//
//  NowPlayingRequestVM.swift
//  BMSDemo
//
//  Created by Kuldeep on 16/10/20.
//  Copyright © 2020 KULDEEP. All rights reserved.
//

import Foundation

struct NowPlayingRequestVM: Validatable {
    var apiKey: String?
    var region: String?
    
    func validate() -> Bool {

        guard let apiKey = self.apiKey else {
            return false
        }
        return !(apiKey.isEmpty)
    }

}
