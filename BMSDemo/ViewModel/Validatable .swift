//
//  Validatable .swift
//  HotCoffee
//
//  Created by Kuldeep on 16/10/20.
//  Copyright © 2020 KULDEEP. All rights reserved.
//

import Foundation

protocol Validatable {
    func validate() -> Bool
}
