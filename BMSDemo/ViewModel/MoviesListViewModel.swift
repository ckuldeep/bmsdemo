//
//  MoviesListViewModel.swift
//  BMSDemo
//
//  Created by Kuldeep on 15/10/20.
//  Copyright © 2020 KULDEEP. All rights reserved.
//

import Foundation

class MoviesListViewModel {
    
    var moviesViewModel: [MovieDataViewModel]
    
    init() {
        self.moviesViewModel = [MovieDataViewModel]()
    }
}

extension MoviesListViewModel {
    
    func movieDataViewModel(at index: Int) -> MovieDataViewModel {
        return self.moviesViewModel[index]
    }
    
}

struct MovieDataViewModel {
    let movieData: MovieDataModel
}

extension MovieDataViewModel {
    var movieName: String {
        return self.movieData.title ?? ""
    }
    
    var posterImg: String {
        return self.movieData.posterPath ?? ""
    }
    
    var releaseDate: String {
        return self.movieData.releaseDate ?? ""
    }
    
    var backDropImg: String {
        return self.movieData.backdropPath ?? ""
    }
    
    var movieId: Int {
        return self.movieData.id ?? 0
    }
    
    var movieOverview: String {
        return self.movieData.overview ?? ""
    }
    
}
