//
//  Utils.swift
//  BMSDemo
//
//  Created by Kuldeep on 16/10/20.
//  Copyright © 2020 KULDEEP. All rights reserved.
//

import UIKit

class Utils {
    
    func getColor(_ color: String?) -> UIColor? {
        let hex = color ?? ""
        return UIColor(hex: hex)
    }
    
}
