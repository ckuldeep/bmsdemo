//
//  UIViewController+Ext.swift
//  BMSDemo
//
//  Created by Kuldeep on 17/10/20.
//  Copyright © 2020 KULDEEP. All rights reserved.
//

import UIKit

extension UIViewController {
    
    static var SegueIdentifier: String {
        return String(describing: Self.self)
    }
    
}
