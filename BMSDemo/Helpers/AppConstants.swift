//
//  AppConstants.swift
//  BMSDemo
//
//  Created by Kuldeep on 15/10/20.
//  Copyright © 2020 KULDEEP. All rights reserved.
//

import Foundation

struct APIKey {
    static let key = "0c915c23b5d84029cd958d8eeba46906"
}

struct Region {
    static let india = "IN"
}

enum AppColors: String, CustomStringConvertible {
    case primary1 = "#3C4959"
    case primary2 = "#F28705"
    case secondary1 = "#F2E205"
    case secondary2 = "#F2B705"
    case secondary3 = "#F27405"
    
    var description: String {
        get {
            return self.rawValue
        }
    }
}
