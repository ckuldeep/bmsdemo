//
//  URL+Extensions.swift
//  HotCoffee
//
//  Created by Mohammad Azam on 4/16/19.
//  Copyright © 2019 Mohammad Azam. All rights reserved.
//

import Foundation

enum PosterSizes: String {
    case tile = "w185"
    case poster = "w500"
}

extension URL {
    
    static var base: String {
        return "https://api.themoviedb.org/3"
    }
    
    static var nowPlaying: URL? {
        return URL(string: base + "/movie/now_playing")
    }
    
    static var reviews: String? {
        return base + "/movie/{Movie_Id}/reviews"
    }

    static func posterImgURL(_ imgStr: String?, size: PosterSizes) -> String {
        return "https://image.tmdb.org/t/p/" + size.rawValue + (imgStr ?? "")
    }
    
}
