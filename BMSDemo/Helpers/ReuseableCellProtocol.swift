//
//  ReuseableCellProtocol.swift
//  BMSDemo
//
//  Created by Kuldeep on 16/10/20.
//  Copyright © 2020 KULDEEP. All rights reserved.
//

import UIKit

protocol ReusableCellProtocol {
    static var ReuseIdentifier: String { get }
    static var NibName: String { get }
}

extension ReusableCellProtocol {
    static var ReuseIdentifier: String {
        return String(describing: Self.self)
    }
    static var NibName: String {
        return String(describing: Self.self)
    }
    static func nib() -> UINib? {
        if NibName.count > 0 {
            return UINib(nibName: NibName, bundle: nil)
        } else {
            return nil
        }
    }
}

extension ReusableCellProtocol where Self: UITableViewCell {
    static func registerNibForTable(_ table: UITableView) {
        if let nib = self.nib() {
            table.register(nib, forCellReuseIdentifier: self.ReuseIdentifier)
        }
    }
}

extension ReusableCellProtocol where Self: UICollectionViewCell {
    static func registerNibForCollection(_ collection: UICollectionView) {
        if let nib = self.nib() {
            collection.register(nib, forCellWithReuseIdentifier: self.ReuseIdentifier)
        }
    }
}
